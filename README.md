# ePublishing Provisioning Playbook

by Mike Green (mgreen@epublishing.com)

An Ansible provisioning playbook and Vagrantfile for running ePub sites in a Vagrant box. It uses the [epub-rails base box](https://atlas.hashicorp.com/mikedamage/boxes/epub-rails/), built by the author.

## Why Do I Need This?

This is a ready-made ePublishing development environment that you can run on any computer. It has minimal dependencies for you to install, so it's easier than manually setting up a machine with all of Jade's many dependencies. If you're on a shared computer, or using someone else's, or just lazy, this project is for you.

## How Does It Work?

Several things happen when you run `vagrant up`:

1. Vagrant downloads the base box, which is a compressed virtual machine image.
1. Vagrant uses the compressed image to create a new virtual machine and boots it.
1. Vagrant then runs the included Ansible playbook to configure the VM.
1. Ansible installs Jade dependencies, sets up the database, and configures the development environment.
1. Profit!

## Prerequisites

+ [Vagrant](http://vagrantup.com)
+ [VirtualBox](http://virtualbox.org)
+ [Ansible](https://www.ansible.com)

You can easily install these programs using [Homebrew Cask](https://caskroom.github.io/).

```bash
brew tap caskroom/cask
brew update
brew cask install vagrant virtualbox
brew install ansible
```

## Setup

1. Clone this repository
1. Edit `Vagrantfile` and tell it where your projects live, as well as which projects to share with the VM. There are comments that tell you what you need to edit.
1. Run `vagrant up`. This will download the VM image, clone it into a running VM, and run the provisioning playbook.
1. Run `vagrant ssh` to connect to the VM.
1. `cd` into a shared app folder and work like normal. Port 3000 is forwarded from the VM to your host machine, so http://localhost:3000 works as normal.
